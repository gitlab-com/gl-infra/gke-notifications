# GKE Notifications
This is a small cloud function that listens on a pubsub topic for messages from a GKE cluster indiciating
a manual or automatic upgrade has been initiated. The function then writes an annotation into Grafana with
specific tags allowing you to easily determine when GKE upgrades have taken place when looking at metrics
in Grafana.

# Setting up a GKE cluster to send notifications
You need to setup a pubsub topic in the same GCP project as the GKE cluster in question, then you need
to follow the instructions at https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-upgrade-notifications
in order to have the cluster send upgrade notifications to the topic.

# Deploying this cloud function
You can deploy this cloud function by referencing the source code which is automatically uploaded to
```
gs://gitlab-gke-notifications-function/gke-notifications.zip
```
You will need to make sure your cloud function has the following environment variables set
* `ENVIRONMENT`: The environment of the cluster (set as a tag on the annotation)
* `GRAFANA_URL`: the URL to the Grafana instance that you are writing annotations to
* `GRAFANA_API_KEY`: The API key used to authenticate to the Grafana instance

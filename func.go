package gkenotifications

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/hashicorp/go-retryablehttp"
)

// PubSubMessage is the payload of a Pub/Sub event.
type PubSubMessage struct {
	Attributes map[string]string `json:"attributes"`
	Data       []byte            `json:"data"`
}

// GrafanaAnnotation is a payload of the annotation sent to Grafana
type GrafanaAnnotation struct {
	Time int64    `json:"time"`
	Text string   `json:"text"`
	Tags []string `json:"tags"`
}

// HelloPubSub consumes a Pub/Sub message.
func HelloPubSub(ctx context.Context, m PubSubMessage) error {

	attributes := m.Attributes
	environment := os.Getenv("ENVIRONMENT")
	payload := make(map[string]interface{})

	log.Printf("Message attributes are, %s", attributes)

	if attributes["type_url"] == "type.googleapis.com/google.container.v1beta1.UpgradeEvent" {
		log.Printf("This is an upgrade event!")

		err := json.Unmarshal([]byte(m.Attributes["payload"]), &payload)
		if err != nil {
			log.Fatal("Error decoding payload to JSON: ", err)
		}

		mtime, err := time.Parse(time.RFC3339, payload["operationStartTime"].(string))
		if err != nil {
			log.Fatal("Error decoding message start time: ", err)
		}

		annotation := GrafanaAnnotation{
			Time: mtime.Unix() * 1000,
			Text: fmt.Sprintf("GKE Cluster %s has begin an auto upgrade operation from version %s to version %s",
				attributes["cluster_name"],
				payload["currentVersion"].(string),
				payload["targetVersion"].(string)),
			Tags: []string{"gkeupgrade", payload["targetVersion"].(string), attributes["cluster_name"], environment},
		}

		jsonToPost, err := json.Marshal(annotation)
		if err != nil {
			log.Fatal("Error converting annotation object to JSON: ", err)
		}
		log.Printf("Json record to post is: %s", jsonToPost)

		postToGrafana(jsonToPost)
	}
	return nil
}

// postToGrafana takes a byte[] of json to post to grafana and sends it as an annotation
func postToGrafana(json []byte) {

	retryClient := retryablehttp.NewClient()
	retryClient.RetryMax = 10

	request, err := retryablehttp.NewRequest("POST", os.Getenv("GRAFANA_URL"), bytes.NewBuffer(json))
	if err != nil {
		log.Fatal("Error creating request object: ", err)
	}
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", os.Getenv("GRAFANA_API_KEY")))
	request.Header.Add("Content-Type", "application/json")

	resp, err := retryClient.Do(request)
	if err != nil {
		log.Fatal("Error talking to Grafana: ", err)
	} else {
		defer resp.Body.Close()
		respStatusCode := strconv.Itoa(resp.StatusCode)
		respBody, _ := ioutil.ReadAll(resp.Body)
		log.Printf("Response from Grafana: Code=%s, Body=%s", respStatusCode, respBody)
	}
}

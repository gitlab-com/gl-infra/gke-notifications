package gkenotifications

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

// TestFunc runs a test of the HelloPubSub function
func TestFunc(t *testing.T) {
	m := PubSubMessage{
		Attributes: map[string]string{
			"cluster_location": "us-east1",
			"cluster_name":     "pre-gitlab-gke",
			"payload":          "{\"resourceType\":\"MASTER\",\"operation\":\"operation-1599777848703-23089ff6\",\"operationStartTime\":\"2020-09-13T23:52:50.947361190Z\",\"currentVersion\":\"1.16.13-gke.400\",\"targetVersion\":\"1.17.9-gke.1503\"}",
			"project_id":       "1079668147158",
			"type_url":         "type.googleapis.com/google.container.v1beta1.UpgradeEvent",
		},
		Data: []byte{},
	}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		fmt.Fprintln(w, "Body received was "+string(body))
	}))
	defer ts.Close()
	os.Setenv("GRAFANA_URL", ts.URL)
	HelloPubSub(*new(context.Context), m)
}
